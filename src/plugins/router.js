import VueRouter from "vue-router";
import Vue from "vue";

Vue.use(VueRouter)
const routes = [
    //{
    //    path:'/login',
    //    component: () => import('../pages/Login')
    //},
    {
        path: '/',
        component: () => import('../pages/Home')
    },
    {
        path: '/team',
        component: () => import('../pages/Team')
    },
    {
        path: '/team2',
        component: () => import('../pages/Team2')
    },
    {
        path: '/user',
        component: () => import('../pages/UserHome')
    },
    {
        path: '/user/course',
        component: () =>import('../pages/UserCours')
    },
    {
        path: '/user/price',
        component: () =>import('../pages/UserPrice')
    },
    {
        path: '/login',
        component: () =>import('../pages/Auth')
    },
    {
        path: '/register',
        component: () =>import('../pages/PRegister')
    }

]

export default new VueRouter({
    mode: 'history' ,
    routes

})
