import axios from "axios";
import store from "./store";
import router from "../router";

axios.defaults.headers.common['Content-Type'] = 'application/ld+json'

axios.interceptors.request.use(
    (config) => {
        if (config.url !== 'http://localhost:8505/api/users/auth') {
            config.headers.common['Authorization'] = 'bearer ' + store.getters.getToken
        }

        return config
    }
)
axios.interceptors.response.use((response) => {
        return response
    }, (error) => {
        if (error.response.status !== 401) {
            return new Promise((resolve, reject) => {
                reject(error)
            })
        } else if (error.response.data.message === "Expired JWT Token") {
            store.dispatch("fetchRefreshToken")
                .then(() => {
                    return router.go(router.currentRoute)
                })
            return new Promise((resolve, reject) => {
                reject(error);
            })

        }
    }
)


export default axios
